const ITERATIONS: u32 = 100_000;
static ALGORITHM: &ring::digest::Algorithm = &ring::digest::SHA256;

pub fn derive(salt: &[u8], password: &[u8]) -> Vec<u8> {
    let mut hash = vec![0u8; ring::digest::SHA256_OUTPUT_LEN];

    ring::pbkdf2::derive(ALGORITHM, ITERATIONS, &salt[..], &password[..], &mut hash);

    hash
}

pub fn verify(salt: &[u8], actual: &[u8], expected: &[u8]) -> bool {
    ring::pbkdf2::verify(ALGORITHM, ITERATIONS, salt, actual, expected).is_ok()
}

#[cfg(test)]
mod tests {
    #[test]
    fn round_trip() {
        let password = b"abc123";
        let salt = b"321cba";
        let hash = super::derive(&salt[..], &password[..]);
        assert!(super::verify(&salt[..], &password[..], &hash[..]));
    }
}
