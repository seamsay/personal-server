use clap::{crate_authors, crate_description, crate_name, crate_version};
use rand::Rng;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = clap::app_from_crate!()
        .help_message("Print help information.")
        .version_message("Print version information.")
        .arg(
            clap::Arg::with_name("salt")
                .short("s")
                .long("salt")
                .help("Season the password.")
                .long_help("Provide a salt for the password. If a salt is not provided one will be generated.")
                .takes_value(true)
        )
        .get_matches();

    let password = rpassword::read_password_from_tty(Some("Password: "))?;
    let password = password.as_bytes();
    let salt = if let Some(salt) = args.value_of("salt") {
        salt.as_bytes().to_owned()
    } else {
        let mut salt = vec![0u8; 64];
        rand::thread_rng().fill(&mut salt[..]);
        salt
    };

    let hash = password::derive(&salt, &password);

    print!("         Salt Hex: ");
    for byte in &salt {
        print!("{:02X}", byte);
    }
    println!();

    print!("Password Hash Hex: ");
    for byte in &hash {
        print!("{:02X}", byte);
    }
    println!();

    Ok(())
}
