use std::hash::{Hash, Hasher};

fn hash<T: Hash>(t: &T) -> u64 {
    let mut s = std::collections::hash_map::DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}

fn db() -> std::path::PathBuf {
    std::path::PathBuf::from(std::env::temp_dir()).join(format!(
        "rust-server-test-sqlite-{}.db",
        hash(&std::thread::current().id()),
    ))
}

fn run(test: impl Fn(rocket::local::Client)) {
    struct RemoveOnDrop<'p> {
        path: &'p std::path::Path,
    }

    impl<'p> Drop for RemoveOnDrop<'p> {
        fn drop(&mut self) {
            // TODO: This always seems to fail, but resources are still cleared up so
            // ¯\_(ツ)_/¯.
            if let Err(error) = std::fs::remove_file(&self.path) {
                eprintln!(
                    "WARNING: RemoveOnDrop::drop failed to remove {}: {:?}",
                    self.path.display(),
                    error,
                );
            }
        }
    }


    let path = db();
    // Just in case the drop fails.
    if path.exists() {
        eprintln!(
            "WARNING: Previous database ({}) still exists!",
            path.display()
        );
        std::fs::remove_file(&path).expect("could not clean up previous database");
    }
    let _ = RemoveOnDrop { path: &path };

    let connection = rusqlite::Connection::open(&path).expect("unable to open database");
    connection
        .execute_batch(include_str!("sqlite.db.sql"))
        .expect("unable to populate database");

    let config = rocket::config::Config::build(rocket::config::Environment::Development)
        .secret_key("8Xui8SN4mI+7egV/9dlfYYLGQJeEx4+DwmSQLwDVXJg=")
        .extra("template_dir", "templates")
        .extra("databases", {
            let mut map = std::collections::HashMap::new();
            map.insert("sqlite", {
                let mut map = std::collections::HashMap::new();
                map.insert(
                    "url",
                    path.to_str().expect("path is not valid UTF-8").to_owned(),
                );
                map
            });
            map
        })
        .finalize()
        .expect("unable to configure test application");

    test(
        rocket::local::Client::new(server::setup(rocket::custom(config)))
            .expect("rocket application invalid"),
    );
}

fn parse(response: &mut rocket::local::LocalResponse) -> scraper::Html {
    let document = response.body_string().expect("response has no body");
    let html = scraper::Html::parse_document(&document);

    if !html.errors.is_empty() {
        for error in html.errors.iter() {
            eprintln!("ERROR: Parsing Document: {:?}", error);
        }

        panic!("Document had parsing errors!");
    }

    html
}

fn login(client: &rocket::local::Client) {
    assert!(client
        .post("/login")
        .header(rocket::http::ContentType::Form)
        .body("username=foo&password=bar")
        .dispatch()
        .cookies()
        .iter()
        .any(|cookie| cookie.name() == "user_id"),);
}


#[test]
fn index_returns_200() {
    run(|client| {
        let response = client.get("/").dispatch();

        assert_eq!(response.status(), rocket::http::Status::Ok);
    });
}

#[test]
fn index_returns_html() {
    run(|client| {
        let response = client.get("/").dispatch();

        assert_eq!(
            response.content_type(),
            Some(rocket::http::ContentType::HTML)
        );
    });
}

#[test]
fn index_provides_login_link_when_not_logged_in() {
    run(|client| {
        let html = parse(&mut client.get("/").dispatch());
        let selector = scraper::Selector::parse("a").unwrap();
        assert!(html.select(&selector).any(|element| {
            element
                .value()
                .attr("href")
                .map(|attr| attr == "/login")
                .unwrap_or(false)
        }));
    });
}

#[test]
fn index_does_not_provide_login_link_when_logged_in() {
    run(|client| {
        login(&client);

        let html = parse(&mut client.get("/").dispatch());
        let selector = scraper::Selector::parse("a").unwrap();

        assert!(html
            .select(&selector)
            .find(|element| {
                element
                    .value()
                    .attr("href")
                    .map(|attr| attr == "/login")
                    .unwrap_or(false)
            })
            .is_none());
    });
}

#[test]
fn index_nav_shows_all_public_links_and_no_private_ones_when_not_logged_in() {
    run(|client| {
        let expected = {
            let mut expected = std::collections::HashSet::new();
            expected.insert("/public");
            expected
        };

        let html = parse(&mut client.get("/").dispatch());
        let nav = scraper::Selector::parse("nav").unwrap();
        let a = scraper::Selector::parse("a").unwrap();

        let actual = {
            let mut actual = std::collections::HashSet::new();
            for nav in html.select(&nav) {
                for a in nav.select(&a) {
                    if let Some(attr) = a.value().attr("href") {
                        actual.insert(attr);
                    }
                }
            }
            actual
        };

        assert_eq!(actual, expected);
    });
}

#[test]
fn index_nav_shows_all_links_when_logged_in() {
    run(|client| {
        login(&client);

        let expected = {
            let mut expected = std::collections::HashSet::new();
            expected.insert("/public");
            expected.insert("/weight");
            expected
        };

        let html = parse(&mut client.get("/").dispatch());
        let nav = scraper::Selector::parse("nav").unwrap();
        let a = scraper::Selector::parse("a").unwrap();

        let actual = {
            let mut actual = std::collections::HashSet::new();
            for nav in html.select(&nav) {
                for a in nav.select(&a) {
                    if let Some(attr) = a.value().attr("href") {
                        actual.insert(attr);
                    }
                }
            }
            actual
        };

        assert_eq!(actual, expected);
    });
}

#[test]
fn login_returns_200() {
    run(|client| {
        let response = client.get("/login").dispatch();

        assert_eq!(response.status(), rocket::http::Status::Ok);
    });
}

#[test]
fn login_returns_html() {
    run(|client| {
        let response = client.get("/login").dispatch();

        assert_eq!(
            response.content_type(),
            Some(rocket::http::ContentType::HTML)
        );
    });
}

#[test]
fn login_provides_form_to_login() {
    run(|client| {
        let html = parse(&mut client.get("/login").dispatch());
        let form = scraper::Selector::parse("form").unwrap();
        let label = scraper::Selector::parse("label").unwrap();
        let input = scraper::Selector::parse("input").unwrap();

        assert!(html.select(&form).any(|form| {
            form.value()
                .attr("action")
                .map(|attr| attr == "/login")
                .unwrap_or(false)
                && form
                    .value()
                    .attr("method")
                    .map(|attr| attr == "post")
                    .unwrap_or(false)
                && form.select(&label).any(|label| {
                    label
                        .value()
                        .attr("for")
                        .map(|attr| attr == "username")
                        .unwrap_or(false)
                })
                && form.select(&input).any(|input| {
                    input
                        .value()
                        .attr("type")
                        .map(|attr| attr == "text")
                        .unwrap_or(false)
                        && input
                            .value()
                            .attr("name")
                            .map(|attr| attr == "username")
                            .unwrap_or(false)
                })
                && form.select(&label).any(|label| {
                    label
                        .value()
                        .attr("for")
                        .map(|attr| attr == "password")
                        .unwrap_or(false)
                })
                && form.select(&input).any(|input| {
                    input
                        .value()
                        .attr("type")
                        .map(|attr| attr == "password")
                        .unwrap_or(false)
                        && input
                            .value()
                            .attr("name")
                            .map(|attr| attr == "password")
                            .unwrap_or(false)
                })
                && form.select(&input).any(|input| {
                    input
                        .value()
                        .attr("type")
                        .map(|attr| attr == "submit")
                        .unwrap_or(false)
                })
        }));
    });
}

#[test]
fn login_with_correct_password_sets_cookie() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=foo&password=bar")
            .dispatch();

        assert!(response
            .cookies()
            .iter()
            .any(|cookie| cookie.name() == "user_id"));
    });
}

#[test]
fn login_with_incorrect_password_does_not_set_cookie() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=foo&password=abc123")
            .dispatch();

        assert!(!response
            .cookies()
            .iter()
            .any(|cookie| cookie.name() == "user_id"));
    });
}

#[test]
fn login_with_incorrect_username_does_not_set_cookie() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=name&password=bar")
            .dispatch();

        assert!(!response
            .cookies()
            .iter()
            // TODO: Is there a way to check the value?
            .any(|cookie| cookie.name() == "user_id"));
    });
}

#[test]
fn login_with_correct_password_redirects() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=foo&password=bar")
            .dispatch();

        assert_eq!(response.status(), rocket::http::Status::SeeOther);
    });
}

#[test]
fn login_with_incorrect_password_redirects() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=foo&password=abc123")
            .dispatch();

        assert_eq!(response.status(), rocket::http::Status::SeeOther);
    });
}

#[test]
fn login_with_incorrect_username_redirects() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=name&password=bar")
            .dispatch();

        assert_eq!(response.status(), rocket::http::Status::SeeOther);
    });
}

#[test]
fn login_with_correct_password_redirects_to_index() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=foo&password=bar")
            .dispatch();

        assert_eq!(
            response.headers().get("location").collect::<Vec<_>>(),
            vec!["/"]
        );
    });
}

#[test]
fn login_with_incorrect_password_redirects_to_login() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=foo&password=abc123")
            .dispatch();

        assert_eq!(
            response.headers().get("location").collect::<Vec<_>>(),
            vec!["/login"]
        );
    });
}

#[test]
fn login_with_incorrect_username_redirects_to_login() {
    run(|client| {
        let response = client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=name&password=bar")
            .dispatch();

        assert_eq!(
            response.headers().get("location").collect::<Vec<_>>(),
            vec!["/login"]
        );
    });
}

#[test]
fn login_with_incorrect_password_shows_flash_message() {
    run(|client| {
        client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=foo&password=abc123")
            .dispatch();

        let html = parse(&mut client.get("/login").dispatch());
        let aside = scraper::Selector::parse("aside").unwrap();

        assert!(html.select(&aside).any(|aside| aside
            .text()
            .any(|message| message == "Invalid username or password!")));
    });
}

#[test]
fn login_with_incorrect_username_shows_flash_message() {
    run(|client| {
        client
            .post("/login")
            .header(rocket::http::ContentType::Form)
            .body("username=name&password=bar")
            .dispatch();

        let html = parse(&mut client.get("/login").dispatch());
        let aside = scraper::Selector::parse("aside").unwrap();

        assert!(html.select(&aside).any(|aside| aside
            .text()
            .any(|message| message == "Invalid username or password!")));
    });
}

#[test]
fn weight_not_accessible_when_not_logged_in() {
    run(|client| {
        let response = client.get("/weight").dispatch();

        assert_eq!(response.status(), rocket::http::Status::Unauthorized);
    });
}

#[test]
fn weight_returns_200() {
    run(|client| {
        login(&client);

        let response = client.get("/weight").dispatch();

        assert_eq!(response.status(), rocket::http::Status::Ok);
    });
}

#[test]
fn weight_returns_html() {
    run(|client| {
        login(&client);

        let response = client.get("/weight").dispatch();

        assert_eq!(
            response.content_type(),
            Some(rocket::http::ContentType::HTML)
        );
    });
}

#[test]
fn weight_provides_form_to_submit_measurement() {
    run(|client| {
        login(&client);

        let html = parse(&mut client.get("/weight").dispatch());
        let form = scraper::Selector::parse("form").unwrap();
        let label = scraper::Selector::parse("label").unwrap();
        let input = scraper::Selector::parse("input").unwrap();

        assert!(html.select(&form).any(|form| {
            form.value()
                .attr("action")
                .map(|attr| attr == "/weight")
                .unwrap_or(false)
                && form
                    .value()
                    .attr("method")
                    .map(|attr| attr == "post")
                    .unwrap_or(false)
                && form.select(&label).any(|label| {
                    label
                        .value()
                        .attr("for")
                        .map(|attr| attr == "date")
                        .unwrap_or(false)
                })
                && form.select(&input).any(|input| {
                    input
                        .value()
                        .attr("type")
                        .map(|attr| attr == "date")
                        .unwrap_or(false)
                        && input
                            .value()
                            .attr("name")
                            .map(|attr| attr == "date")
                            .unwrap_or(false)
                })
                && form.select(&label).any(|label| {
                    label
                        .value()
                        .attr("for")
                        .map(|attr| attr == "measurement")
                        .unwrap_or(false)
                })
                && form.select(&input).any(|input| {
                    input
                        .value()
                        .attr("type")
                        .map(|attr| attr == "number")
                        .unwrap_or(false)
                        && input
                            .value()
                            .attr("name")
                            .map(|attr| attr == "measurement")
                            .unwrap_or(false)
                })
                && form.select(&input).any(|input| {
                    input
                        .value()
                        .attr("type")
                        .map(|attr| attr == "submit")
                        .unwrap_or(false)
                })
        }));
    });
}

#[test]
fn weight_post_redirects() {
    run(|client| {
        login(&client);

        let response = client
            .post("/weight")
            .header(rocket::http::ContentType::Form)
            .body("date=2020-08-17&measurement=80.5")
            .dispatch();

        assert_eq!(response.status(), rocket::http::Status::SeeOther);
    });
}

#[test]
fn weight_post_redirects_to_weight() {
    run(|client| {
        login(&client);

        let response = client
            .post("/weight")
            .header(rocket::http::ContentType::Form)
            .body("date=2020-08-17&measurement=80.5")
            .dispatch();

        assert_eq!(
            response.headers().get("location").collect::<Vec<_>>(),
            vec!["/weight"]
        );
    });
}

#[test]
fn weight_post_saves_measurement_to_database() {
    run(|client| {
        login(&client);

        let path = db();
        let connection = rusqlite::Connection::open(&path).expect("unable to open database");

        let before = connection
            .query_row_and_then("SELECT COUNT(*) FROM weight_measurements", &[], |row| {
                row.get_checked::<_, i32>(0)
            })
            .unwrap();

        client
            .post("/weight")
            .header(rocket::http::ContentType::Form)
            .body("date=2020-08-17&measurement=80.5")
            .dispatch();

        let after = connection
            .query_row_and_then("SELECT COUNT(*) FROM weight_measurements", &[], |row| {
                row.get_checked::<_, i32>(0)
            })
            .unwrap();

        assert_eq!(after, before + 1);
    });
}

#[test]
fn weight_post_with_invalid_date_fails() {
    run(|client| {
        login(&client);

        let response = client
            .post("/weight")
            .header(rocket::http::ContentType::Form)
            .body("date=hello&measurement=80.5")
            .dispatch();

        assert_eq!(response.status(), rocket::http::Status::InternalServerError);
    });
}

#[test]
fn weight_post_with_invalid_measurement_fails() {
    run(|client| {
        login(&client);

        let response = client
            .post("/weight")
            .header(rocket::http::ContentType::Form)
            .body("date=2020-06-12&measurement=hello")
            .dispatch();

        assert_eq!(response.status(), rocket::http::Status::UnprocessableEntity);
    });
}

#[test]
fn weight_post_with_invalid_date_does_not_save_to_db() {
    run(|client| {
        login(&client);

        let path = db();
        let connection = rusqlite::Connection::open(&path).expect("unable to open database");

        let before = connection
            .query_row_and_then("SELECT COUNT(*) FROM weight_measurements", &[], |row| {
                row.get_checked::<_, i32>(0)
            })
            .unwrap();

        client
            .post("/weight")
            .header(rocket::http::ContentType::Form)
            .body("date=2020--17&measurement=80.5")
            .dispatch();

        let after = connection
            .query_row_and_then("SELECT COUNT(*) FROM weight_measurements", &[], |row| {
                row.get_checked::<_, i32>(0)
            })
            .unwrap();

        assert_eq!(after, before);
    });
}

#[test]
fn weight_post_with_invalid_measurement_does_not_save_to_db() {
    run(|client| {
        login(&client);

        let path = db();
        let connection = rusqlite::Connection::open(&path).expect("unable to open database");

        let before = connection
            .query_row_and_then("SELECT COUNT(*) FROM weight_measurements", &[], |row| {
                row.get_checked::<_, i32>(0)
            })
            .unwrap();

        client
            .post("/weight")
            .header(rocket::http::ContentType::Form)
            .body("date=2020-08-17&measurement=8.0.5")
            .dispatch();

        let after = connection
            .query_row_and_then("SELECT COUNT(*) FROM weight_measurements", &[], |row| {
                row.get_checked::<_, i32>(0)
            })
            .unwrap();

        assert_eq!(after, before);
    });
}
