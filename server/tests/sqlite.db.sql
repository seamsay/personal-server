PRAGMA foreign_keys=ON;
BEGIN TRANSACTION;
CREATE TABLE users (
    user_id INTEGER PRIMARY KEY,
    username TEXT NOT NULL UNIQUE,
    name TEXT NOT NULL,
    salt BLOB NOT NULL,
    hash BLOB NOT NULL
);
-- Password is `bar`.
INSERT INTO users VALUES(1,'foo','Foo Bar',X'85ed0ccfcbf37559aabb5dfeee17e5b38c113b5c6966b206ef413cc82324d32520d6303cb53492a0155d0eeedb07f4e602d81dde838dbe556c218898183fe112',X'968f2c8f21798b3c0353014c9eb8ab51cf7d099178294722dbc730e4ffcc98c5');
CREATE TABLE weight_measurements (
    weight_measurement_id INTEGER PRIMARY KEY,
    user_id REFERENCES users,
    date TEXT NOT NULL CHECK (date is DATE(date)),
    measurement_kg REAL NOT NULL
);
INSERT INTO weight_measurements VALUES(1,1,'2020-01-19',83.549999999999997159);
INSERT INTO weight_measurements VALUES(2,1,'2020-01-20',83.500000000000000003);
COMMIT;
