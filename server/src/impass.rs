use impass::encrypted;
use rocket_contrib::databases::r2d2;
use rocket_contrib::templates::tera::Context;
use rocket_contrib::templates::Template;
use std::collections::HashMap;
use std::path::PathBuf;

#[derive(Debug)]
pub struct Connection {
    age: usize,
    database: encrypted::Decrypted,
    password: Vec<u8>,
    path: PathBuf,
}

impl Connection {
    fn new(path: PathBuf, password: Vec<u8>) -> Result<Self, Error> {
        let file = std::fs::File::open(&path)?;
        let encrypted = impass::Encrypted::load(&file)?;
        let decrypted = encrypted.decrypt(&password)?;

        Ok(Connection {
            age: 0,
            database: decrypted,
            password,
            path,
        })
    }
}

impl Connection {
    pub fn get(&self) -> &impass::Database {
        &self.database.database
    }

    pub fn modify<F>(&mut self, age: usize, operation: F) -> Result<(), ()>
    where
        F: FnOnce(&mut Self),
    {
        if self.age != age {
            return Err(());
        }

        operation(self);
        // TODO: Save after operation.

        self.age += 1;
        Ok(())
    }
}

#[derive(Debug)]
pub struct Manager {
    password: Vec<u8>,
    path: PathBuf,
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    Decryption(#[from] encrypted::DecryptionError),
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error(transparent)]
    Load(#[from] encrypted::LoadError),
    #[error(transparent)]
    R2d2(#[from] r2d2::Error),
}

impl r2d2::ManageConnection for Manager {
    type Connection = Connection;
    type Error = Error;

    fn connect(&self) -> Result<Self::Connection, Self::Error> {
        Self::Connection::new(self.path.clone(), self.password.clone())
    }

    fn is_valid(&self, _: &mut Self::Connection) -> Result<(), Self::Error> {
        Ok(())
    }

    fn has_broken(&self, _: &mut Self::Connection) -> bool {
        false
    }
}

impl rocket_contrib::databases::Poolable for Connection {
    type Error = r2d2::Error;
    type Manager = Manager;

    fn pool(
        config: rocket_contrib::databases::DatabaseConfig,
    ) -> Result<r2d2::Pool<Self::Manager>, Self::Error> {
        // TODO: Handle missing password.
        // TODO: Handle passwords specified as Arrays of integers.
        let password = config
            .extras
            .get("password")
            .expect("no password found")
            .as_str()
            .map(str::as_bytes)
            .map(Vec::from)
            .expect("passwords must be UTF-8");

        let manager = Manager {
            password,
            path: PathBuf::from(config.url),
        };

        r2d2::Pool::builder()
            .max_size(config.pool_size)
            .build(manager)
    }
}

fn _view_group(
    user: &str,
    group: &impass::database::Group,
    encoded: &[&str],
    decoded: &[std::borrow::Cow<'_, str>],
    context: Context,
) -> Template {
    let segments = decoded
        .iter()
        .enumerate()
        .map(|(i, component)| {
            let name = component.to_string();
            let href = encoded[..i + 1].join("/");

            [("name", name), ("href", href)]
                .iter()
                .cloned()
                .collect::<HashMap<_, _>>()
        })
        .collect::<Vec<_>>();

    let url = encoded
        .iter()
        .map(|segment| format!("{}/", segment))
        .collect::<String>();

    let groups = {
        let mut groups = group
            .groups()
            .iter()
            .map(|(key, _)| {
                let name = key.to_string();
                // TODO: Relax NON_ALPHANUMERIC (https://url.spec.whatwg.org/#fragment-percent-encode-set).
                let href =
                    percent_encoding::utf8_percent_encode(key, percent_encoding::NON_ALPHANUMERIC)
                        .to_string();

                [("name", name), ("href", href)]
                    .iter()
                    .cloned()
                    .collect::<HashMap<_, _>>()
            })
            .collect::<Vec<_>>();

        groups.sort_unstable_by_key(|group| group["name"].clone());
        groups
    };

    let entries = {
        let mut entries = group
            .entries()
            .iter()
            .map(|(key, _)| {
                let name = key.to_string();
                // TODO: Relax NON_ALPHANUMERIC (https://url.spec.whatwg.org/#fragment-percent-encode-set).
                let href =
                    percent_encoding::utf8_percent_encode(key, percent_encoding::NON_ALPHANUMERIC)
                        .to_string();

                [("name", name), ("href", href)]
                    .iter()
                    .cloned()
                    .collect::<HashMap<_, _>>()
            })
            .collect::<Vec<_>>();

        entries.sort_unstable_by_key(|entry| entry["name"].clone());
        entries
    };

    let context = {
        let mut context = context;

        context.insert("user", &user);
        context.insert("segments", &segments);
        context.insert("url", &url);
        context.insert("groups", &groups);
        context.insert("entries", &entries);

        context
    };

    Template::render("impass-view-group", &context)
}

fn _view_entry(
    user: &str,
    entry: &impass::database::Entry,
    encoded: &[&str],
    decoded: &[std::borrow::Cow<'_, str>],
    context: Context,
) -> Template {
    let segments = decoded
        .iter()
        .enumerate()
        .map(|(i, component)| {
            let name = component.to_string();
            let href = encoded[..i + 1].join("/");

            [("name", name), ("href", href)]
                .iter()
                .cloned()
                .collect::<HashMap<_, _>>()
        })
        .collect::<Vec<_>>();

    let fields = {
        let mut fields = entry
            .fields()
            .iter()
            .map(|(key, field)| {
                use impass::database::FieldValue::*;

                let name = key;
                let value = match field.value() {
                    Ok(Utf8(value)) => value,
                    Ok(Binary(_)) => todo!("use mime to create the html tag"),
                    Err(_) => todo!("display to user"),
                };
                let hints = field.hints();
                let mime = format!("{}", field.mime());

                let context = {
                    let mut context = Context::new();

                    context.insert("name", &name);
                    context.insert("value", &value);
                    context.insert("hints", &hints);
                    context.insert("mime", &mime);

                    context
                };

                (name.to_string(), context)
            })
            .collect::<Vec<_>>();

        fields.sort_unstable_by_key(|field| field.0.clone());

        fields
            .into_iter()
            .map(|(_, field)| field)
            .collect::<Vec<_>>()
    };

    let context = {
        let mut context = context;

        context.insert("user", &user);
        context.insert("segments", &segments);
        context.insert("fields", &fields);

        context
    };

    Template::render("impass-view-entry", &context)
}

fn _view(
    user: u32,
    database: &rusqlite::Connection,
    passwords: &Connection,
    path: &[&str],
    context: Context,
) -> eyre::Result<Result<Template, rocket::response::Flash<rocket::response::Redirect>>> {
    let encoded = path;
    let decoded = encoded
        .iter()
        .map(|segment| percent_encoding::percent_decode_str(segment).decode_utf8())
        .collect::<Result<Vec<_>, _>>()?;

    let user = database.query_row_and_then(
        "SELECT name FROM users WHERE user_id = ?1",
        &[&user],
        |r| r.get_checked::<_, String>("name"),
    )?;

    if let Some(group) = passwords.get().hierarchy.at(&decoded) {
        Ok(Ok(_view_group(&user, &group, &encoded, &decoded, context)))
    } else if let Some(entry) = decoded
        .split_last()
        .and_then(|(entry_name, group_path)| {
            passwords
                .get()
                .hierarchy
                .at(&group_path)
                .map(|group| (entry_name, group))
        })
        .and_then(|(entry_name, group)| group.entries().get(&entry_name.to_string()))
        .map(|&entry_id| &passwords.get().entries()[entry_id])
    {
        Ok(Ok(_view_entry(&user, &entry, &encoded, &decoded, context)))
    } else {
        Ok(Err(rocket::response::Flash::error(
            rocket::response::Redirect::to(uri!(view_root)),
            format!("No entry or group at `{:?}`!", decoded),
        )))
    }
}

#[get("/impass")]
pub fn view_root(
    user: super::User,
    database: super::Database,
    passwords: super::PasswordManager,
    flash: Option<rocket::request::FlashMessage>,
) -> eyre::Result<Result<Template, rocket::response::Flash<rocket::response::Redirect>>> {
    let mut context = Context::new();

    if let Some(flash) = flash {
        context.insert("flash", flash.msg());
    }

    _view(user.0, &database.0, &passwords.0, &[], context)
}

#[get("/impass/<path..>")]
pub fn view(
    user: super::User,
    database: super::Database,
    passwords: super::PasswordManager,
    path: rocket::http::uri::Segments,
) -> eyre::Result<Result<Template, rocket::response::Flash<rocket::response::Redirect>>> {
    _view(
        user.0,
        &database.0,
        &passwords.0,
        &path.collect::<Vec<_>>(),
        Context::new(),
    )
}
