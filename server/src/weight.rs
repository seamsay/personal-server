use rocket::request;
use rocket::response;

#[derive(FromForm, Debug)]
pub struct WeightMeasurement {
    date: String,
    measurement: f64,
}

#[get("/weight")]
pub fn view(
    user: super::User,
    database: super::Database,
) -> Result<super::Template, rusqlite::Error> {
    let name = database.query_row_and_then(
        "SELECT name FROM users WHERE user_id = ?1",
        &[&user.0],
        |r| r.get_checked::<_, String>("name"),
    )?;

    let measurements = database
        .prepare(
            "SELECT date, measurement_kg FROM weight_measurements WHERE user_id = ?1 ORDER BY date",
        )?
        .query_and_then::<_, rusqlite::Error, _>(&[&user.0], |row| {
            let date = row.get_checked::<_, String>("date")?;
            let measurement = row.get_checked::<_, f64>("measurement_kg")?;
            Ok((date, measurement))
        })?
        .collect::<Result<Vec<_>, _>>()?;

    let context = {
        let mut context = super::Context::new();
        context.insert("user", &name);
        context.insert("measurements", &measurements);
        context.insert("uri", &uri!(add).path());
        context
    };

    Ok(super::Template::render("weight", &context))
}

#[post("/weight", data = "<measurement>")]
pub fn add(
    user: super::User,
    database: super::Database,
    measurement: request::Form<WeightMeasurement>,
) -> Result<response::Redirect, rusqlite::Error> {
    database
        .execute(
            "INSERT INTO weight_measurements (user_id, date, measurement_kg) VALUES (?1, ?2, ?3)",
            &[&user.0, &measurement.date, &measurement.measurement],
        )
        .map(|_| response::Redirect::to(uri!(view)))
}
