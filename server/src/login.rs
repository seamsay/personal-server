use rocket::http;
use rocket::request;
use rocket::response;

#[derive(FromForm, Debug)]
pub struct Login {
    username: String,
    password: String,
}

#[get("/login")]
pub fn view(
    database: super::Database,
    flash: Option<request::FlashMessage>,
    mut cookies: http::Cookies,
) -> Result<super::Template, rusqlite::Error> {
    let context = {
        let mut context = super::Context::new();

        if let Some(name) = cookies
            .get_private(super::USER_ID_COOKIE_NAME)
            .map(|cookie| cookie.value().to_owned())
            .map(|id| {
                database.query_row_and_then(
                    "SELECT name FROM users WHERE user_id = ?1",
                    &[&id],
                    |row| row.get_checked::<_, String>("name"),
                )
            })
            .transpose()?
        {
            context.insert("user", &name);
        }

        if let Some(msg) = flash.map(|f| f.msg().to_owned()) {
            context.insert("flash", &msg);
        }

        context.insert("uri", &uri!(login).path());

        context
    };

    Ok(super::Template::render("login", &context))
}

#[post("/login", data = "<login>")]
pub fn login(
    database: super::Database,
    login: request::Form<Login>,
    mut cookies: http::Cookies,
) -> Result<response::Redirect, Result<response::Flash<response::Redirect>, rusqlite::Error>> {
    let (id, salt, expected) = match database.query_row_and_then(
        "SELECT user_id, salt, hash FROM users WHERE username = ?1",
        &[&login.username],
        |row| {
            let user = row.get_checked::<_, u32>("user_id")?;
            let salt = row.get_checked::<_, Vec<u8>>("salt")?;
            let hash = row.get_checked::<_, Vec<u8>>("hash")?;
            Ok((user, salt, hash))
        },
    ) {
        Ok(values) => values,
        Err(rusqlite::Error::QueryReturnedNoRows) => {
            return Err(Ok(response::Flash::error(
                response::Redirect::to(uri!(view)),
                "Invalid username or password!",
            )))
        }
        Err(error) => return Err(Err(error)),
    };

    if password::verify(&salt, login.password.as_bytes(), &expected) {
        cookies.add_private(http::Cookie::new(
            super::USER_ID_COOKIE_NAME,
            id.to_string(),
        ));
        Ok(response::Redirect::to(uri!(crate::index)))
    } else {
        Err(Ok(response::Flash::error(
            response::Redirect::to(uri!(view)),
            "Invalid username or password!",
        )))
    }
}
