#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

use rocket::http;
use rocket::request;
use rocket_contrib::templates::tera::Context;
use rocket_contrib::templates::Template;

mod impass;
mod login;
mod weight;

const USER_ID_COOKIE_NAME: &str = "user_id";

#[derive(Debug)]
pub struct User(u32);

impl<'a, 'r> request::FromRequest<'a, 'r> for User {
    type Error = Option<std::num::ParseIntError>;

    fn from_request(request: &'a request::Request<'r>) -> request::Outcome<Self, Self::Error> {
        use rocket::http::Status;
        use rocket::request::Outcome;

        match request
            .cookies()
            .get_private(USER_ID_COOKIE_NAME)
            .map(|c| c.value().parse().map(User))
        {
            Some(Ok(user)) => Outcome::Success(user),
            Some(Err(error)) => Outcome::Failure((Status::InternalServerError, Some(error))),
            None => Outcome::Failure((Status::Unauthorized, None)),
        }
    }
}

// TODO: Enable foreign keys on each connection.
#[database("sqlite")]
#[derive(Debug)]
pub struct Database(rusqlite::Connection);

#[database("impass")]
#[derive(Debug)]
pub struct PasswordManager(impass::Connection);

#[get("/")]
fn index(database: Database, mut cookies: http::Cookies) -> Result<Template, rusqlite::Error> {
    let context = {
        let mut context = Context::new();
        if let Some(name) = cookies
            .get_private(USER_ID_COOKIE_NAME)
            .map(|cookie| cookie.value().to_owned())
            .map(|id| {
                database.query_row_and_then(
                    "SELECT name FROM users WHERE user_id = ?1",
                    &[&id],
                    |row| row.get_checked::<_, String>("name"),
                )
            })
            .transpose()?
        {
            context.insert("user", &name);
        }
        context
    };
    Ok(Template::render("index", &context))
}

pub fn setup(rocket: rocket::Rocket) -> rocket::Rocket {
    rocket
        .attach(Database::fairing())
        .attach(PasswordManager::fairing())
        .attach(Template::fairing())
        .mount(
            "/",
            rocket::routes![
                index,
                impass::view_root, impass::view,
                login::view, login::login,
                weight::view, weight::add,
            ],
        )
        .mount(
            "/public",
            rocket_contrib::serve::StaticFiles::from(
                rocket::config::Config::active()
                    .expect("invalid environment")
                    .get_str("public_dir")
                    .or_else(|error| {
                        if let rocket::config::ConfigError::Missing(_) = error {
                            Ok("")
                        } else {
                            Err(error)
                        }
                    })
                    .unwrap(),
            ),
        )
}
