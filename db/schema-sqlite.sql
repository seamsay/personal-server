CREATE TABLE users (
    user_id INTEGER PRIMARY KEY,
    username TEXT NOT NULL UNIQUE,
    name TEXT NOT NULL,
    salt BLOB NOT NULL,
    hash BLOB NOT NULL
);

CREATE TABLE weight_measurements (
    weight_measurement_id INTEGER PRIMARY KEY,
    user_id REFERENCES users,
    date TEXT NOT NULL CHECK (date IS DATE(date)),
    measurement_kg REAL NOT NULL
);
